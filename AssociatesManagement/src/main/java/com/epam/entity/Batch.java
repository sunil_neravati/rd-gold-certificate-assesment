package com.epam.entity;

import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "batches")
public class Batch {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "batch_id")
	private int id;
	
	@Column(name = "batch_name",unique = true)
	private String name;
	
	private String practice;
	
	private Date startDate;
	private Date endDate;
	
	@OneToMany(mappedBy = "batch",cascade = CascadeType.ALL ,orphanRemoval = true)
	private List<Associate> associates;
	
}
