package com.epam.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDto;
import com.epam.service.AssociateService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/rd/associates")
@RequiredArgsConstructor
public class AssociateController {
	
	private final AssociateService associateService;
	
	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDTO){
		return new ResponseEntity<>(associateService.addAssociate(associateDTO),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removeAssociate(@PathVariable int id){
		associateService.deleteAssociate(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping
	public ResponseEntity<AssociateDto> updateAssociate(@RequestBody @Valid AssociateDto associateDTO){
		return new ResponseEntity<>(associateService.updateAssociate(associateDTO),HttpStatus.OK);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> findAllAssociatesByGender(@PathVariable String gender){
		return new ResponseEntity<>(associateService.findAssociatesByGender(gender),HttpStatus.OK);
	}
	

}
