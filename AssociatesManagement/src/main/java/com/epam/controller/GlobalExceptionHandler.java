package com.epam.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.dto.ExceptionResponse;
import com.epam.exception.AssociateNotFound;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> methodArgumentExceptionHandling(
			MethodArgumentNotValidException methodArgumentNotValidException, WebRequest request) {
		Map<String, String> result = new HashMap<>();
		methodArgumentNotValidException.getBindingResult().getFieldErrors()
				.forEach(error -> result.put(error.getField(), error.getDefaultMessage()));
		log.error(ExceptionUtils.getStackTrace(methodArgumentNotValidException));
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				result.toString(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> runtimeExceptionHandling(RuntimeException runtimeException,
			WebRequest request) {
		log.error(ExceptionUtils.getStackTrace(runtimeException));
		return new ResponseEntity<>(
				new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
						runtimeException.getMessage(), request.getDescription(false)),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ExceptionResponse> sqlExceptionHandling(
			DataIntegrityViolationException dataIntegrityViolationException, WebRequest request) {
		String message = "Batch already exists";
		log.error(ExceptionUtils.getStackTrace(dataIntegrityViolationException));
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), message,
				request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(AssociateNotFound.class)
	public ResponseEntity<ExceptionResponse> assocaiateExceptionHandling(AssociateNotFound associateException,
			WebRequest request) {
		log.error(ExceptionUtils.getStackTrace(associateException));
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.name(),
				associateException.getMessage(), request.getDescription(false)), HttpStatus.NOT_FOUND);
	}

}
