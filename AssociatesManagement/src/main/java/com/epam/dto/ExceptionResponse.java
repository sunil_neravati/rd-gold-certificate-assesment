package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ExceptionResponse {
	
	private String timeStamp;
	private String errorMessage;
	private String status;
	private String path;

}
