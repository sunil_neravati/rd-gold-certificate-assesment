package com.epam.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssociateDto {
	
	private int id;
	
	@NotBlank(message = "Name field is mandatory")
	private String name;
	
	@Email(message = "provide email in xyz@gmail.com")
	private String email;
	
	@Size(max = 1, message = "Gender should be either M/F")
	private String gender;
	
	private String college;
	
	@NotBlank(message = "status should not be blank")
	private String status;
	
	@NotNull(message = "Batch should not be null")
	private BatchDto batch;

}
