package com.epam.dto;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BatchDto {
	
	private int id;
	
	@NotBlank(message = "Name should not be blank")
	private String name;
	
	@NotBlank(message = "practice field is mandatory")
	private String practice;
	
	private Date startDate;
	
	private Date endDate;

}
