package com.epam.service;

import java.util.List;

import com.epam.dto.AssociateDto;

public interface AssociateService {
	
	AssociateDto addAssociate(AssociateDto associateDTO);
	
	void deleteAssociate(int id);
	
	AssociateDto updateAssociate(AssociateDto associateDTO);
	
	List<AssociateDto> findAssociatesByGender (String gender);
	
}
