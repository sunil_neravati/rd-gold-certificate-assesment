package com.epam.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateNotFound;
import com.epam.exception.GenderException;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {
	
	private final AssociateRepository associateRepository;
	private final BatchRepository batchRepository;
	private final ModelMapper modelMapper;

	@Override
	public AssociateDto addAssociate(AssociateDto associateDTO) {
		
		Associate associate = converToAssociateEntity(associateDTO);
		Batch batch = associate.getBatch();
		if(!batchRepository.existsById(batch.getId())) {
			batchRepository.save(batch);
		}
		associate.setBatch(batch);
		
		return convertToAssociateDto(associateRepository.save(associate));
		
	}

	

	@Override
	public void deleteAssociate(int id) {
		associateRepository.deleteById(id);
	}

	@Override
	public AssociateDto updateAssociate(AssociateDto associateDTO) {
		
		if(!associateRepository.existsById(associateDTO.getId())) {
			throw new AssociateNotFound("Associate not found with given id ");
		}
		
		Associate associate = converToAssociateEntity(associateDTO);
		Batch batch = associate.getBatch();
		
		if(!batchRepository.existsById(batch.getId())) {
			batchRepository.save(batch);
		}
		associate.setBatch(batch);
		
		return convertToAssociateDto(associateRepository.save(associate));
		
	}



	private AssociateDto convertToAssociateDto(Associate associate) {
		return modelMapper.map(associate,AssociateDto.class);
	}

	@Override
	public List<AssociateDto> findAssociatesByGender(String gender) {
		
		if(!(gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F"))) {
			
			throw new GenderException("Gender should be M/f");
		}
		return associateRepository.findAllByGender(gender)
				     .stream()
				     .map(associate -> modelMapper.map(associate, AssociateDto.class))
				     .toList();
	}
	
	private Associate converToAssociateEntity(AssociateDto associateDTO) {
		return modelMapper.map(associateDTO, Associate.class);
	}
	
	

}
