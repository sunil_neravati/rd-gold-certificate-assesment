package com.epam.exception;

public class GenderException extends RuntimeException{
	
	public GenderException(String message) {
		
		super(message);
	}

}
