package com.epam.exception;

public class AssociateNotFound extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5097274685580001475L;
	
	public AssociateNotFound(String message) {
		super(message);
	}

}
