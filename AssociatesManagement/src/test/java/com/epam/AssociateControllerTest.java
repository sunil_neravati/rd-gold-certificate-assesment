package com.epam;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociateDto;
import com.epam.dto.BatchDto;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateNotFound;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class AssociateControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AssociateService associateService;

	private Associate associate;

	private AssociateDto associateDTO;

	private Batch batch;

	private BatchDto batchDTO;

	@BeforeEach
	void setUp() {

		batch = new Batch();
		batch.setAssociates(Arrays.asList(associate));
		batch.setEndDate(new Date(22));
		batch.setStartDate(new Date(20));
		batch.setPractice("Java");
		batch.setName("kevin");

		batchDTO = new BatchDto();
		batchDTO.setEndDate(new Date(22));
		batchDTO.setStartDate(new Date(20));
		batchDTO.setPractice("Java");
		batchDTO.setName("kevin");

		associate = new Associate();
		associate.setCollege("GPREC");
		associate.setEmail("sunil@epam.com");
		associate.setGender("M");
		associate.setId(1);
		associate.setStatus("Active");
		associate.setName("sunil");
		associate.setBatch(batch);

		associateDTO = new AssociateDto();

		associateDTO.setBatch(batchDTO);
		associateDTO.setCollege("Vignan");
		associateDTO.setEmail("sunil@epam.com");
		associateDTO.setGender("M");
		associateDTO.setId(1);
		associateDTO.setStatus("Active");
		associateDTO.setName("sunil");

	}

	@Test
	void addAssociate() throws JsonProcessingException, Exception {
		Mockito.when(associateService.addAssociate(any(AssociateDto.class))).thenReturn(associateDTO);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate))).andExpect(status().isCreated());

		Mockito.verify(associateService).addAssociate(any(AssociateDto.class));
	}

	@Test
	void addAssociateWithSameExistingBatchName() throws JsonProcessingException, Exception {
		Mockito.when(associateService.addAssociate(any(AssociateDto.class)))
				.thenThrow(DataIntegrityViolationException.class);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate))).andExpect(status().isBadRequest());

		Mockito.verify(associateService).addAssociate(any(AssociateDto.class));
	}

	@Test
	void deleteAssociate() throws JsonProcessingException, Exception {

		mockMvc.perform(delete("/rd/associates/{id}", "x")).andExpect(status().isInternalServerError());

	}

	@Test
	void deleteAssociateWithId() throws JsonProcessingException, Exception {

		mockMvc.perform(delete("/rd/associates/{id}", 1)).andExpect(status().isNoContent());

	}

	@Test
	void updateAssociate() throws JsonProcessingException, Exception {
		Mockito.when(associateService.updateAssociate(any(AssociateDto.class))).thenReturn(associateDTO);

		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate))).andExpect(status().isOk());

		Mockito.verify(associateService).updateAssociate(any(AssociateDto.class));
	}

	@Test
	void updateAssociateWithInvalidId() throws JsonProcessingException, Exception {
		Mockito.when(associateService.updateAssociate(any(AssociateDto.class)))
				.thenThrow(new AssociateNotFound("AssociateNot Found"));

		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate))).andExpect(status().isNotFound());

		Mockito.verify(associateService).updateAssociate(any(AssociateDto.class));
	}

	@Test
	void updateAssociateWithInvalidParameters() throws JsonProcessingException, Exception {

		associate.setGender("MALe");
		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate))).andExpect(status().isBadRequest());

	}

	@Test
	void findAllAssociatesByGender() throws JsonProcessingException, Exception {
		Mockito.when(associateService.findAssociatesByGender("M")).thenReturn(List.of(associateDTO));

		mockMvc.perform(get("/rd/associates/{gender}", "M")).andExpect(status().isOk());

		Mockito.verify(associateService).findAssociatesByGender("M");
	}

}
