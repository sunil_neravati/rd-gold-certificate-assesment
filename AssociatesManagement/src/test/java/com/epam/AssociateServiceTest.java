package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.AssociateDto;
import com.epam.dto.BatchDto;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateNotFound;
import com.epam.exception.GenderException;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.impl.AssociateServiceImpl;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {

	

	@Mock
	private BatchRepository batchRepository;

	@Mock
	private AssociateRepository associateRepository;

	@Mock
	private ModelMapper modelMapper;

	@InjectMocks
	private AssociateServiceImpl associateService;
	
	
	private Associate associate;

	private AssociateDto associateDTO;

	private Batch batch;

	private BatchDto batchDTO;
	
	
	
	@BeforeEach
	void setUpAssociate() {
		
		
		batch = new Batch();
		batch.setAssociates(Arrays.asList(associate));
		batch.setEndDate(new Date(22));
		batch.setStartDate(new Date(20));
		batch.setPractice("Java");
		batch.setName("kevin");


		batchDTO = new BatchDto();
		batchDTO.setEndDate(new Date(22));
		batchDTO.setStartDate(new Date(20));
		batchDTO.setPractice("Java");
		batchDTO.setName("kevin");

		
		associate = new Associate();
		associate.setCollege("GPREC");
		associate.setEmail("sunny@epam.com");
		associate.setGender("M");
		associate.setId(1);
		associate.setStatus("ACTIVE");
		associate.setName("sunil");
		associate.setBatch(batch);

		
		associateDTO = new AssociateDto();

		associateDTO.setBatch(batchDTO);

		associateDTO.setCollege(associate.getCollege());

		associateDTO.setGender(associate.getGender());

		associateDTO.setId(1);
		associateDTO.setStatus(associate.getStatus());
		associateDTO.setName(associate.getName());
		associateDTO.setEmail(associate.getEmail());
		
		

	}


	@Test
	void testAddAssociateWithValidData() {
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(true);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDTO, associateService.addAssociate(associateDTO));
	}

	@Test
	void testAddAssociateWithNewBatch() {
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(false);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDTO, associateService.addAssociate(associateDTO));
	}

	@Test
	void testRemoveAssociate() {
		Mockito.doNothing().when(associateRepository).deleteById(1);
		associateService.deleteAssociate(1);
		Mockito.verify(associateRepository).deleteById(1);
	}

	@Test
	void testUpdateAssociate() {
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(true);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(associateRepository.existsById(1)).thenReturn(true);
		assertEquals(associateDTO, associateService.updateAssociate(associateDTO));
	}

	@Test
	void testUpdateAssociateWithNewBatch() {
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(false);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(associateRepository.existsById(1)).thenReturn(true);
		assertEquals(associateDTO, associateService.updateAssociate(associateDTO));
	}

	@Test
	void testByGender() {

		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDTO);
		Mockito.when(associateRepository.findAllByGender("M")).thenReturn(List.of(associate));

		assertTrue(associateService.findAssociatesByGender("M").size() > 0);
	}

	@Test
	void testGenderWithInvalidInput() {

		assertThrows(GenderException.class, () -> associateService.findAssociatesByGender("x"));

	}

	@Test
	void testUpdateWithInnvalidId() {
		Mockito.when(associateRepository.existsById(1)).thenReturn(false);
		assertThrows(AssociateNotFound.class, () -> associateService.updateAssociate(associateDTO));
	}

	
	
}
